google-api-python-client~=2.9.0

aiogram~=2.13

aiocron~=1.6

python-dateutil~=2.8.1
pytz~=2021.1

peewee~=3.14.4
