# YouTube Notifications

This is telegram bot that utilizes YouTube Data API to notify users about new uploads from tracked channels

## Implemented

- Getting uploads list of channel via YouTube Data API
- Bot, that can:
    - Send messages to specific users or to everyone
    - Register new users
    - Add/remove channels from tracking
- Cron task to check for updates on tracked channels every 5 minutes and send notifications through bot about new videos
- SQLite database to store all settings
- Individual and shared modes (partially, not really tested)
- Multi-message commands that persist between bot launches*

*You can send first part (actual command, e.g. `/subscribe`), then bot can restart, then you can send next part (e.g. link to subscribe to) and so on


## To do

- Some kind of static file to store all bot-specific strings (e.g. containing bot name, version etc.)
- Proper app setup and settings modification through a command line
- Bot commands to search for channels and then subscribe to them ??
- Filtering for video names ???????????????
- Rework readme

## Notes

Currently, most messages sent by bot contain bot-specific strings. I will address this in static file part of todo
