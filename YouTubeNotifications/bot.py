"""Telegram bot to send notifications"""

__all__ = ['run', 'send_message', 'send_to_everyone']

from .database import *
from .utilities import truncate
from .FutureMessageHandler import FutureMessageHandler
from . import youtube as yt

import asyncio
from aiogram import Bot, Dispatcher, executor, exceptions, types as tg

from random import choice

from typing import Iterable

from traceback import format_exc


import logging
log = logging.getLogger('bot')

version = '0.4'

with db:
    bot = Bot(Settings.default.token)
dispatcher = Dispatcher(bot)


handler = FutureMessageHandler()


# Dispatcher message handlers

@dispatcher.message_handler(commands=['start', 's'])
async def say_hello(message: tg.Message):
    """Welcome new user"""

    with db:
        if User.get_or_none(userId=message.from_user.id) is not None:
            return await message.answer(
                'You have already started, haven\'t you?')

        log.info(f'New user: {message.from_user.id} | '
                 f'{message.from_user.username}')

        if message.from_user.username == Settings.default.admin:
            admin = True
        else:
            admin = False

        User.create(userId=message.from_user.id,
                    name=message.from_user.username,
                    isAdmin=admin)

        await message.answer('HALLO!\nI am Hermitcraft Bot! '
                             '<i>(read with iskall85\'s voice)</i>',
                             parse_mode='HTML')


@dispatcher.message_handler(commands=['help', 'h'])
async def send_help(message: tg.Message):
    """Send help"""

    if authorized(message.from_user.id, 'help'):
        commands = [
            ('help', '/help, /h - Get this message'),

            ('version', '/version, /v - Get current bot version'),

            ('subscriptions', '/subscriptions - View list of channels '
                              'you will get notifications for'),

            ('subscribe', '/subscribe or /subscribe [link] '
                          '- add new channel to tracking'),

            ('unsubscribe', '/unsubscribe or /unsubscribe [link or name] '
                            '- remove channel from tracking'),
            ('latest', '/latest or /latest [x] - view 10 or x '
                       'latest videos from hermits')
        ]

        help_msg = 'These are commands you can use:\n\n' + \
                   '\n'.join(desc for com, desc in commands
                             if authorized(message.from_user.id, com)) + \
                   '\n\nWell, that\'s it... for now...'

        return await message.answer(help_msg)

    await not_authorized(message)


@dispatcher.message_handler(commands=['version', 'v'])
async def send_version(message: tg.Message):
    """Send current bot version"""

    if authorized(message.from_user.id, 'version'):
        return await message.answer(f'My version is: {version}')

    await not_authorized(message)


@dispatcher.message_handler(commands=['subscriptions'])
async def subscriptions(message: tg.Message):
    """View subscription list"""

    if authorized(message.from_user.id, 'subscriptions'):
        with db:
            if Settings.default.individual:
                u = User.get(userId=message.from_user.id)

                if u is None:
                    return await message.answer(
                        'Sorry, you are not in my database for some reason. '
                        'Please use /start command and I\'ll add you')

                subs = u.subscriptions

            else:
                subs = Subscription.select()

            if subs.exists():

                text = f'There are currently {len(subs)} hermits ' \
                       f'playing in Season 8. Here they are:\n\n' + \
                       '\n'.join(
                           f'<a href="{sub.url}"><b>{sub.name}</b></a>'
                           for sub in subs
                       )

            else:
                if Settings.default.individual or \
                        User.get(userId=message.from_user.id).isAdmin:

                    text = 'You are not subscribed to any channel :(\n' \
                           'But fear not! Because I\'ve got a /subscribe ' \
                           'command for you.\n' \
                           'Give it a try :)'

                else:
                    text = 'Admin has not filled subscriptions list yet'

        return await message.answer(text, parse_mode='HTML')

    await not_authorized(message)


@dispatcher.message_handler(commands=['subscribe'])
async def subscribe(message: tg.Message):
    """Add channel to subscriptions"""

    if authorized(message.from_user.id, 'subscribe'):
        content = message.text.replace('/subscribe', '').strip()

        if len(content) > 0:
            return await subscribe_to(message, link=content)

        try:
            handler[message.from_user.id] = subscribe_to

        except KeyError:
            return await message.answer(
                'Sorry, you are not in my database for some reason. '
                'Please use /start command and I\'ll add you'
            )

        return await message.answer(
            'What channel do you want to subscribe to?\n'
            '<i>(send me a link)</i>',
            parse_mode='HTML'
        )

    await not_authorized(message)


@dispatcher.message_handler(commands=['unsubscribe'])
async def unsubscribe(message: tg.Message):
    """Remove channel from subscriptions"""

    if authorized(message.from_user.id, 'unsubscribe'):
        content = message.text.replace('/unsubscribe', '').strip()

        if len(content) > 0:
            return await unsubscribe_to(message, link_or_name=content)

        try:
            handler[message.from_user.id] = unsubscribe_to

        except KeyError:
            return await message.answer(
                'Sorry, you are not in my database for some reason. '
                'Please use /start command and I\'ll add you'
            )

        return await message.answer(
            'What channel do you want to unsubscribe from?\n'
            '<i>(send me name or link)</i>',
            parse_mode='HTML'
        )

    await not_authorized(message)


@dispatcher.message_handler(commands=['latest'])
async def latest(message: tg.Message):
    """View latest videos"""

    if authorized(message.from_user.id, 'latest'):
        content = message.text.replace('/latest', '').strip()

        num = 10

        if content != '':
            try:
                num = int(content)
            except ValueError:
                await message.answer(
                    'This number doesn\'t seem right to me. I\'ll use 10 then')

        with db:
            through = User.subscriptions.get_through_model()
            query = Upload.select().join(Subscription).join(through).join(User)

            if Settings.default.individual:
                u = User.get_or_none(userId=message.from_user.id)

                if u is None:
                    return await message.answer(
                        'Sorry, you are not in my database for some reason. '
                        'Please use /start command and I\'ll add you'
                    )

                query = query.where(User.id == u.id)

            result = query.order_by(Upload.published.desc()).limit(num)

            text = 'Latest videos from hermits:\n\n' + '\n\n'.join(
                f'<b>{up.sub.name}</b> - <a href="{up.url}">{up.name}</a>'
                for up in result
            )

            return await message.answer(
                text, parse_mode='HTML',
                disable_web_page_preview=True
            )

    else:
        await not_authorized(message)


@dispatcher.message_handler()
async def normal_text(message: tg.Message):
    """Responses to messages not recognized as commands"""

    if message.text[0] == '/':
        return await message.answer(f'I do not recognize this command')

    if await handler.handle(message.from_user.id, message):
        return

    # This is for testing and fun
    log.info(f'{message.from_user.username} says: {message.text}')

    await message.answer(
        ''.join(map(choice, zip(message.text.lower(), message.text.upper())))
    )


# FutureMessageHandler callbacks
@handler
async def subscribe_to(message: tg.Message, *, link: str = None):
    """Actual subscription process"""

    if message is None:
        raise ValueError('Message must not be None!')

    if link:
        url = link

    else:
        url = message.text

    try:
        title, playlist_id = yt.get_channel_info(**yt.parse_channel_url(url))

    except ValueError:
        return await message.answer(
            'This link doesn\'t seem right to me... Can you double check?'
        )

    except NotImplementedError:
        return await message.answer(
            'I cannot work with this kind of links yet.\n'
            'Can you please use link like next two?\n\n'
            'https://www.youtube.com/user/USERNAME\n'
            'https://www.youtube.com/channel/MANYLETTERSANDNUMBERS111'
        )

    except FileNotFoundError:
        return await message.answer(
            'I can\' seem to find this channel. '
            'Are you sure the link is correct?')

    except Exception:
        return await message.answer(
            'Something went very wrong. '
            'Here some programmers stuff:\n\n<pre>' +
            format_exc() + '</pre>', parse_mode='HTML'
        )

    else:
        with db:
            u = User.get_or_none(userId=message.from_user.id)

            if u is None:
                return await message.answer(
                    'Sorry, you are not in my database for some reason. '
                    'Please use /start command and I\'ll add you'
                )

            s = Subscription.get_or_none(playlist=playlist_id)

            if s is not None:
                if not Settings.default.individual or s in u.subscriptions:
                    return await message.answer(
                        'You are already subscribed to this channel. '
                        'No, you can not do this twice'
                    )

            else:
                s = Subscription.create(
                    name=title, url=url, playlist=playlist_id
                )

            u.subscriptions.add(s)
            u.save()

            await message.answer(
                f'I added <b>{title}</b> to your subscription list. Yay!',
                parse_mode='HTML'
            )


@handler
async def unsubscribe_to(message: tg.Message, *, link_or_name: str = None):
    """Actual unsubscription process"""

    if message is None:
        raise ValueError('Message must not be None!')

    if link_or_name:
        query = link_or_name

    else:
        query = message.text.strip()  # Just in case

    with db:
        u = User.get_or_none(userId=message.from_user.id)

        if u is None:
            return await message.answer(
                'Sorry, you are not in my database for some reason. '
                'Please use /start command and I\'ll add you'
            )

        s = Subscription.get_or_none(name=query)

        if s is None:
            s = Subscription.get_or_none(url=query)

            if s is None:
                return await message.answer(
                    'Cannot find this channel in your subscriptions. '
                    'Can you double check?'
                )

        if Settings.default.individual:
            if s not in u.subscriptions:
                return await message.answer(
                    'Cannot find this channel in your subscriptions. '
                    'Can you double check?'
                )

            u.subscriptions.remove(s)
            u.save()

            if not s.subscribers.exsists():
                s.delete_instance()

        else:
            for u in s.subscribers:
                u.subscriptions.remove(s)
                u.save()

            s.delete_instance()

        return await message.answer(
            'Done! You won\'t get notifications for this channel anymore'
        )


# Message sending

async def send_message(user_id: int, text: str,
                       disable_notification: bool = False,
                       parse_mode: str = 'HTML') -> bool:
    """
    Send message to specified user

    :param user_id: Telegram user id
    :param text: Message text
    :param disable_notification: Whether to disable notification for this message
    :param parse_mode: Mode to parse message with (markdown, html etc.)
    :return: whether message was successfully sent
    """

    try:
        await bot.send_message(user_id, text, parse_mode=parse_mode,
                               disable_notification=disable_notification)

    except exceptions.BotBlocked:
        log.error(f"Target [ID:{user_id}]: blocked by user")
        # TODO remove id from list?
    except exceptions.ChatNotFound:
        log.error(f"Target [ID:{user_id}]: invalid user ID")
        # TODO remove id from list?
    except exceptions.RetryAfter as e:
        log.error(f"Target [ID:{user_id}]: Flood limit is exceeded. "
                  f"Sleep {e.timeout} seconds.")
        await asyncio.sleep(e.timeout)
        return await send_message(user_id, text)  # Recursive call
    except exceptions.UserDeactivated:
        log.error(f"Target [ID:{user_id}]: user is deactivated")
    except exceptions.TelegramAPIError:
        log.exception(f"Target [ID:{user_id}]: failed")
    else:
        log.info(f"Target [ID:{user_id}]: success")
        return True
    return False


async def send_to_everyone(text: str,
                           disable_notification: bool = False,
                           parse_mode: str = 'HTML'):
    """
    Send message to every registered user

    :param text: Message text
    :param disable_notification: Whether to disable notification for this message
    :param parse_mode: Mode to parse message with (markdown, html etc.)
    """
    with db:
        for user in User.select():
            await send_message(
                user.userId, text,

                disable_notification,
                parse_mode
            )


async def send_to_each(text: str, ids: Iterable[int],
                       disable_notification: bool = False,
                       parse_mode: str = 'HTML'):
    """
    Send message to each user specified

    :param text: Message text
    :param ids: Iterable of user ids to send message to
    :param disable_notification: Whether to disable notification for this message
    :param parse_mode: Mode to parse message with (markdown, html etc.)
    """
    for user_id in ids:
        await send_message(
            user_id, text,

            disable_notification,
            parse_mode
        )


def run(event_loop):
    """Run bot on given event loop"""
    executor.start_polling(dispatcher, loop=event_loop)


# Authorization

public = ['help', 'version', 'subscriptions', 'latest']
individual = ['subscribe', 'unsubscribe']


def authorized(user_id: int, command: str) -> bool:
    """
    Checks if user is authorized to use given command

    :param user_id: Telegram user id
    :param command: Command to check
    :return: Whether user is authorized
    """
    if command in public:
        return True

    with db:
        u = User.get_or_none(userId=user_id)

        if u is not None and u.isAdmin:
            return True

        if command in individual and Settings.default.individual:
            return True

    return False


async def not_authorized(message: tg.Message):
    """Sends 'Not authorized' message in reply"""

    await message.answer(
        'Sorry, but you are not authorized to use this command'
    )
