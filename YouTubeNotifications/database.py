"""Database related content"""

__all__ = ['db', 'Settings', 'Subscription', 'User', 'HandlerRecord', 'Upload']

from peewee import *

from class_property import class_property

from datetime import datetime
import os


db_path = os.path.join(os.path.dirname(__file__), 'db.sqlite')

db = SqliteDatabase(db_path)


class BaseModel(Model):
    class Meta:
        database = db


class Settings(BaseModel):
    """Stores app settings"""
    name = CharField(default='default')
    api_key = FixedCharField(39)
    token = FixedCharField(46)
    admin = CharField()
    individual = BooleanField()

    @class_property
    def default(self) -> 'Settings':
        """Default settings profile"""
        return Settings.get(name='default')

    def __repr__(self):
        return f'Settings({self.name} | {self.api_key}, {self.token})'

    __str__ = __repr__


class Subscription(BaseModel):
    """Channel to track uploads from"""
    name = CharField()
    url = CharField()
    playlist = CharField(default='')
    update_date = DateTimeField(default=datetime.utcnow())

    def __repr__(self):
        return f'Subscription({self.name} | {self.url})'

    __str__ = __repr__


class User(BaseModel):
    """User to send notifications to"""
    userId = IntegerField(unique=True)
    name = CharField(unique=True)
    isAdmin = BooleanField(default=False)
    subscriptions = ManyToManyField(Subscription, 'subscribers')

    def __repr__(self):
        return f'User({self.userId})'

    __str__ = __repr__


class HandlerRecord(BaseModel):
    """To store names of message handlers registered for users"""
    func = CharField()
    user = ForeignKeyField(User, backref='handler')
    # ^ This field should be unique but it works anyways

    def __repr__(self):
        return f'HandlerRecord({self.func} | {self.user.name})'

    __str__ = __repr__


class Upload(BaseModel):
    videoId = CharField()
    name = CharField()
    published = DateTimeField()

    sub = ForeignKeyField(Subscription, backref='uploads')

    @property
    def url(self):
        return 'http://youtu.be/' + self.videoId

    def __repr__(self):
        return f'Upload({self.name} | {self.url} | {self.published})'

    __str__ = __repr__


__tables__ = [
    Settings,
    Subscription,
    User,
    HandlerRecord,
    Upload,

    # Apparently, peewee doesn't create _through table despite stated otherwise
    # Found only closed issue (https://github.com/coleifer/peewee/issues/1624)
    # Fine. I'll do it myself
    User.subscriptions.get_through_model()
]


def create_tables():
    """Creates all tables that should be in database"""
    return db.create_tables(__tables__)
