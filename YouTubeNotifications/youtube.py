"""Contains functions regarding YouTube, e.g. api calls, link conversion etc."""

__all__ = ['make_service', 'parse_channel_url', 'get_uploads_playlist_id',
           'get_last_videos']

from .database import *

from googleapiclient.discovery import build

from typing import Optional, Dict


import logging
log = logging.getLogger('youtube')


def make_service(api_key):
    """
    Create Google API Service for youtube
    :param api_key: API key for service to use
    :return: Created service
    """
    log.info('Creating YouTube service')
    return build('youtube', 'v3', developerKey=api_key)


# Create default service
with db:
    yt_service = make_service(Settings.default.api_key)


def parse_channel_url(url: str) -> Dict[str, str]:
    """
    Retrieves second argument for `get_upload_playlist_id` from user/channel url

    Currently supports either `/user` link or `/channel` link
    (e.g. https://www.youtube.com/c/CustomUrl will not work)

    :param url: Url to get id/username for
    :return: Dictionary to unpack into `get_upload_playlist_id` call
    """
    url = url.strip()  # just in case

    if 'youtube.com' not in url:
        raise ValueError('Not a youtube link')

    # TODO parsing of /c/ links
    if '/c/' in url:
        raise NotImplementedError('Cannot parse such links yet')

    *tokens, id_or_username = url.split('/')

    if 'channel' in tokens:
        return {'channel_id': id_or_username}

    elif 'user' in tokens:
        return {'username': id_or_username}

    else:
        raise ValueError('Not a channel or user link')


def get_channel_info(youtube=yt_service, *,
                     channel_id: Optional[str] = None,
                     username: Optional[str] = None):
    """
    Returns channel title and playlist id for given id or username

    Either `channel_id` or `username` must be provided

    :param youtube: Youtube service instance
    :param channel_id: Id of channel
    :param username: Username (returns info for associated channel)
    :return: Channel info
    """

    if channel_id is None and username is None:
        raise ValueError('Either channel_id or username must be provided')

    if channel_id is not None:
        request = youtube.channels().list(
            part='contentDetails, snippet',
            id=channel_id
        )

    else:
        request = youtube.channels().list(
            part='contentDetails, snippet',
            forUsername=username
        )

    log.info('Requesting channel info')
    response = request.execute()

    try:
        playlist_id = response['items'][0]['contentDetails'] \
            ['relatedPlaylists']['uploads']

        channel_title = response['items'][0]['snippet']['title']

        return channel_title, playlist_id

    except (KeyError, IndexError):
        raise FileNotFoundError('No channel found')


def get_uploads_playlist_id(youtube=yt_service, *,
                            channel_id: Optional[str] = None,
                            username: Optional[str] = None) -> str:
    """
    Returns id of playlist containing given user/channel uploads

    Either `channel_id` or `username` must be provided

    :param youtube: Youtube service instance
    :param channel_id: Id of channel
    :param username: Username (returns playlist for associated channel)
    :return: Playlist id
    """

    _, playlist_id = get_channel_info(
        youtube=youtube,
        channel_id=channel_id,
        username=username
    )

    return playlist_id


def get_last_videos(youtube=yt_service,
                    playlist_id: Optional[str] = None,
                    *, max_count=5):
    """
    Gen info about latest (actually first, but it works as latest)
    videos from playlist

    :param youtube: Youtube service instance
    :param playlist_id: Playlist id
    :param max_count: Max number of videos to retrieve
    :return:
    """

    if playlist_id is None:
        raise ValueError('playlist_id must be provided')

    request = youtube.playlistItems().list(
        part='snippet',
        playlistId=playlist_id,
        maxResults=max_count
    )

    log.info(f'Requesting last videos from {playlist_id}')
    response = request.execute()
    try:
        log.info(f'{len(response["items"])} videos retrieved')
        return [item['snippet'] for item in response['items']]

    except KeyError:    # Probably no videos on channel?
        log.info('No videos retrieved')
        return []
