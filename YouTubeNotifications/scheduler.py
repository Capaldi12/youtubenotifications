"""Regular tasks for bot to perform"""

__all__ = ['check_for_updates']

from . import bot, loop, youtube as yt
from .utilities import *
from .database import *

from aiocron import crontab

from datetime import datetime
from pytz import utc


import logging
log = logging.getLogger('scheduler')


async def check_for_updates():
    """Checks for new videos from specified channels"""

    log.info('Checking for updates')

    with db:
        for sub in Subscription.select():
            update_date = datetime.utcnow()

            log.info(f'Checking {sub.name}')

            # set playlist id if not set.. probably will be removed
            #  with implementation of adding channel through bot command
            if sub.playlist == '':
                sub.playlist = yt.get_uploads_playlist_id(
                    **yt.parse_channel_url(sub.url)
                )

                sub.save()  # don't forget to save

            videos = yt.get_last_videos(playlist_id=sub.playlist)

            for video in reversed(videos):
                published = parse_yt_date(video['publishedAt'])

                up = Upload.get_or_none(videoId=video['resourceId']['videoId'])

                if up is None:
                    Upload.create(
                        videoId=video['resourceId']['videoId'],
                        name=video['title'],
                        published=published,
                        sub=sub
                    )

                # Localize so both have tzinfo.. datetime is hard
                if published > utc.localize(sub.update_date):

                    log.info(f'New video found for {sub.name}. Sending...')

                    if Settings.default.individual:
                        await bot.send_to_each(
                            make_video_message(video),
                            [u.userId for u in sub.subscribers]
                        )

                    else:
                        await bot.send_to_everyone(make_video_message(video))

            sub.update_date = update_date
            sub.save()


# Register task manually
tasks = [
    crontab('*/5 * * * *', loop=loop)(check_for_updates)
]

log.info(f'{len(tasks)} task scheduled')
