"""My solution to persistent multi-message operations"""

__all__ = ['FutureMessageHandler']

from .database import *

from typing import Dict, Callable, Union, Optional


class FutureMessageHandler:
    """
    Frankenstein monster which acts as wrapper to HandlerRecord table
    and callbacks dispatcher. Used to handle commands that might require
    additional input in separate message
    """
    callbacks: Dict[str, Callable] = {}

    def __init__(self):
        self.callbacks = {}

    def __setitem__(self, key: Union[str, int], value: Union[str, Callable]):
        """Set value as callback function with given name
        or as callback record of given user"""

        # Creating callback with given name
        if isinstance(key, str):
            if not isinstance(value, Callable):
                raise ValueError('Value must be callable for str key')

            self.callbacks[key] = value

        # Assigning callback to user
        elif isinstance(key, int):
            if isinstance(value, str):
                name = value

            # If got callable, infer name
            elif isinstance(value, Callable):
                name = self.name(value)

            else:
                raise ValueError('Value must be string or callable for int key')

            # Make sure we have callback to call
            if name not in self.callbacks:
                raise ValueError('Callback not found. Did you register it?')

            with db:
                u = User.get_or_none(userId=key)
                if u is None:
                    raise KeyError('No such user')

                if u.handler.exists():
                    u.handler.get().delete_instance()

                HandlerRecord.create(func=name, user=u)
        else:
            raise ValueError('Key must be either str or int')

    def __getitem__(self, key: Union[str, int]) \
            -> Union[Optional[str], Callable]:
        """Get callback function with given name
        or callback record of given user"""
        if isinstance(key, str):
            try:
                return self.callbacks[key]
            except KeyError as e:
                raise KeyError('No callback with this name') from e

        elif isinstance(key, int):
            with db:
                u = User.get_or_none(userId=key)
                if u is None:
                    raise KeyError('No such user')

                if u.handler.exists():
                    return u.handler.get().func

                return None

        else:
            raise ValueError('Key must be either str or int')

    def __delitem__(self, key: Union[str, int]):
        """Remove callback function with given name
        or callback record of given user"""
        if isinstance(key, str):
            try:
                del self.callbacks[key]
            except KeyError as e:
                raise KeyError('No callback with this name') from e

        elif isinstance(key, int):
            with db:
                u = User.get_or_none(userId=key)
                if u is None:
                    raise KeyError('No such user')

                if u.handler.exists():
                    u.handler.get().delete_instance()

        else:
            raise ValueError('Key must be either str or int')

    def get(self, key, default=None):
        """get item or default if no item with given key"""

        try:
            return self[key]

        except KeyError:
            return default

    def __call__(self, func_or_name: Union[Callable, str]):
        """As decorator"""
        if isinstance(func_or_name, Callable):
            self[self.name(func_or_name)] = func_or_name

            return func_or_name

        elif isinstance(func_or_name, str):
            def decorator(func: Callable):
                self[func_or_name] = func

                return func

            return decorator

        else:
            raise ValueError(f'{type(func_or_name)} '
                             f'is not valid function or name')

    async def handle(self, user_id: int, *args, **kwargs):
        """
        Call handler registered for given user with provided arguments

        :param user_id: Telegram user id
        :param args: Ordinal arguments
        :param kwargs: Keyword arguments
        :return: Whether call was handled
        """
        with db:
            h = self[user_id]
            if h is None:
                return False

            if cb := self.get(h):
                await cb(*args, **kwargs)
                del self[user_id]

                return True

            return False

    @staticmethod
    def name(func: Callable):
        try:
            return func.__name__

        except AttributeError:
            return repr(func)


# This many lines and this is just auxiliary class... smh
