import logging
logging.basicConfig(level=logging.INFO)  # Set logging level for module


from . import bot, loop, \
    scheduler  # scheduler imported to create tasks
from .database import db
from .utilities import delayed

import sys


log = logging.getLogger('__main__')


if len(sys.argv) == 1:
    with db:
        if not db.table_exists('settings'):
            log.error('Please set up application before running bot\n'
                      'Run this module with --setup flag')

    log.info('Starting')

    try:
        loop.create_task(delayed(2, scheduler.check_for_updates()))
        bot.run(loop)

    finally:
        log.info('Finishing')   # Not working :(

elif sys.argv[1] == '--setup':
    ...  # TODO setup here
