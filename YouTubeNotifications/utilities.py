"""Other useful functions"""

__all__ = ['make_video_message', 'parse_yt_date', 'delayed', 'truncate']

from datetime import datetime
import dateutil.parser

import asyncio


def make_video_message(video: dict) -> str:
    """Formats video data into message for bot to send"""
    template = "New video from <i>{channel}</i>\n" \
               "<b>{title}</b>\n\n" \
               "Watch here: http://youtu.be/{video_id}"

    return template.format(
        channel=video['channelTitle'],
        title=video['title'],
        video_id=video['resourceId']['videoId']
    )


def parse_yt_date(date_str) -> datetime:
    """Parses date returned by API"""
    return dateutil.parser.isoparse(date_str)


async def delayed(delay, coro):
    """Starts coroutine with given delay"""
    await asyncio.sleep(delay)
    return await coro


def truncate(string: str, max_len: int):
    if len(string) < max_len:
        return string

    return string[:max_len-3] + '...'
